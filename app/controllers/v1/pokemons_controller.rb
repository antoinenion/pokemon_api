class V1::PokemonsController < ApplicationController
  before_action :set_pokemon, only: [:show, :update, :destroy]

  def index
    @pokemons = Pokemon.paginate(page: params[:page], per_page: params[:per_page])
    render json: @pokemons
  end

  def create
    @pokemon = Pokemon.create!(pokemon_params)
    render json: { id: @pokemon.id }, status: :created
  end

  def show
    render json: @pokemon
  end

  def update
    @pokemon.update(pokemon_params)
    render json: @pokemon
  end

  def destroy
    @pokemon.destroy
    head :no_content
  end

  private

  def pokemon_params
    params.permit(:serial_number, :name, :type_1, :type_2, :total, :attack, :defense, :hit_points, :sp_attack, :sp_defense, :speed, :generation, :legendary)
  end

  def set_pokemon
    @pokemon = Pokemon.find(params[:id])
  end
end
