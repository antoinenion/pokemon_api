require "securerandom"

class Pokemon < ApplicationRecord
  self.per_page = 20

  validates :name, presence: true
  validates :serial_number, presence: true, numericality: true
  validates :type_1, presence: true
  validates :total, presence: true, numericality: true
  validates :attack, presence: true, numericality: true
  validates :defense, presence: true, numericality: true
  validates :hit_points, presence: true, numericality: true
  validates :sp_attack, presence: true, numericality: true
  validates :sp_defense, presence: true, numericality: true
  validates :speed, presence: true, numericality: true
  validates :generation, presence: true, numericality: true, inclusion: { in: 1..6 }

  attribute :id, :string, default: -> { SecureRandom.uuid }
  attribute :legendary, :boolean, default: -> { false }

end
