class PokemonSerializer < ActiveModel::Serializer
  attributes :id, :serial_number, :name, :type_1, :type_2, :total, :attack, :defense, :hit_points, :sp_attack, :sp_defense, :speed, :generation, :legendary
end
