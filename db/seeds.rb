require 'csv'

CSV.foreach(Rails.root.join('db/pokemons_data.csv'), headers: true) do |row|
  Rails.logger.debug 'Import pokemon with Name ' + row['Name']
  pokemon = Pokemon.find_or_create_by(name: row['Name']) do |p|
    p.serial_number = row['#']
    p.name = row['Name']
    p.type_1 = row['Type 1']
    p.type_2 = row['Type 2']
    p.total = row['Total'].to_i
    p.hit_points = row['HP'].to_i
    p.attack = row['Attack'].to_i
    p.defense = row['Defense'].to_i
    p.sp_attack = row['Sp. Atk'].to_i
    p.sp_defense = row['Sp. Def'].to_i
    p.speed = row['Speed'].to_i
    p.generation = row['Generation'].to_i
    p.legendary = (row['Legendary'] == 'True')
  end
end
