# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_01_22_182528) do

  create_table "pokemons", id: :string, force: :cascade do |t|
    t.integer "serial_number"
    t.string "name"
    t.string "type_1"
    t.string "type_2"
    t.string "total"
    t.integer "attack"
    t.integer "defense"
    t.integer "hit_points"
    t.integer "sp_attack"
    t.integer "sp_defense"
    t.integer "speed"
    t.integer "generation"
    t.boolean "legendary"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_pokemons_on_name"
  end

end
