class CreatePokemons < ActiveRecord::Migration[6.0]
  def change
    create_table :pokemons, id: :string do |t|
      t.integer :serial_number
      t.string :name, index: true
      t.string :type_1
      t.string :type_2
      t.integer :total
      t.integer :attack
      t.integer :defense
      t.integer :hit_points
      t.integer :sp_attack
      t.integer :sp_defense
      t.integer :speed
      t.integer :generation
      t.boolean :legendary, default: false
      t.timestamps
    end
  end
end
