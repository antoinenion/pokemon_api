# Dockerfile
FROM ruby:2.6.6

ENV RAILS_ENV production

WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN bundle install

ADD . /app

RUN bundle exec rake db:migrate
RUN bundle exec rake db:seed

EXPOSE 3000

CMD ["rails", "server", "-e", "production",  "-b", "0.0.0.0"]
