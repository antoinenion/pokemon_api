require "rails_helper"

RSpec.describe Pokemon, type: :model do

  subject { FactoryBot.build(:pokemon) }

  it 'returns valid pokemon' do
    expect(subject).to be_valid
  end

  describe '#name' do
    it 'failed when missing' do
      subject.name = nil
      expect(subject).to_not be_valid
    end
  end

  describe '#serial_number' do
    it 'failed when missing' do
      subject.serial_number = nil
      expect(subject).to_not be_valid
    end

    it 'failed when string' do
      subject.serial_number = 'string'
      expect(subject).to_not be_valid
    end
  end

  describe '#type_1' do
    it 'failed when missing' do
      subject.type_1 = nil
      expect(subject).to_not be_valid
    end
  end

  describe '#type_2' do
    it 'validate presence when set' do
      expect(subject.type_2).to_not be_nil
    end

    it 'validate presence when set' do
      subject.type_2 = nil
      expect(subject.type_2).to be_nil
    end
  end

  describe '#total' do
    it 'failed when missing' do
      subject.total = nil
      expect(subject).to_not be_valid
    end

    it 'failed when string' do
      subject.total = 'string'
      expect(subject).to_not be_valid
    end
  end

  describe '#attack' do
    it 'failed when missing' do
      subject.attack = nil
      expect(subject).to_not be_valid
    end

    it 'failed when string' do
      subject.attack = 'string'
      expect(subject).to_not be_valid
    end
  end

  describe '#defense' do
    it 'failed when missing' do
      subject.defense = nil
      expect(subject).to_not be_valid
    end

    it 'failed when string' do
      subject.defense = 'string'
      expect(subject).to_not be_valid
    end
  end

  describe '#hit_points' do
    it 'failed when missing' do
      subject.hit_points = nil
      expect(subject).to_not be_valid
    end

    it 'failed when string' do
      subject.hit_points = 'string'
      expect(subject).to_not be_valid
    end
  end

  describe '#sp_attack' do
    it 'failed when missing' do
      subject.sp_attack = nil
      expect(subject).to_not be_valid
    end

    it 'failed when string' do
      subject.sp_attack = 'string'
      expect(subject).to_not be_valid
    end
  end

  describe '#sp_defense' do
    it 'failed when missing' do
      subject.sp_defense = nil
      expect(subject).to_not be_valid
    end

    it 'failed when string' do
      subject.sp_defense = 'string'
      expect(subject).to_not be_valid
    end
  end

  describe '#speed' do
    it 'failed when missing' do
      subject.speed = nil
      expect(subject).to_not be_valid
    end

    it 'failed when string' do
      subject.speed = 'string'
      expect(subject).to_not be_valid
    end
  end

  describe '#generation' do
    it 'failed when missing' do
      subject.generation = nil
      expect(subject).to_not be_valid
    end

    it 'failed when string' do
      subject.generation = 'string'
      expect(subject).to_not be_valid
    end

    it 'failed when not in range' do
      subject.generation = 7
      expect(subject).to_not be_valid
    end
  end

  describe '#legendary' do
    it 'working when missing' do
      subject.legendary = nil
      expect(subject).to be_valid
    end
  end
end
