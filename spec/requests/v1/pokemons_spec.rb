require 'rails_helper'

RSpec.describe "Api::V1::Pokemons", type: :request do
  describe "GET /v1/pokemons" do
    before {
      create_list(:pokemon, 30)
    }

    it "should return valid payload for pokemons" do
      get "/v1/pokemons"
      expect(response).to be_successful
    end

    it "should return the default max number of items (20)" do
      get "/v1/pokemons"
      expect(response).to be_successful
      expect(JSON.parse(response.body).length).to equal 20
    end

    it "should return the defined number of items" do
      per_page = 2
      get "/v1/pokemons", params: { :per_page => per_page }
      expect(response).to be_successful
      expect(JSON.parse(response.body).length).to equal per_page
    end

    it "should return the last page when page is set" do
      page = 2
      get "/v1/pokemons", params: { :page => page }
      expect(response).to be_successful
      expect(JSON.parse(response.body).length).to equal 10
    end
  end

  describe "POST /v1/pokemons" do
    before {
      @pokemon = build(:pokemon)
      @params = @pokemon.as_json(:except => [:id, :created_at, :updated_at])
    }

    it "should successfully create a pokemon" do
      post "/v1/pokemons", params: @params
      expect(response).to be_successful
    end

  end

  describe "PATCH /v1/pokemons/:id" do
    before {
      @pokemon = create(:pokemon)
    }

    it "should successfully update a pokemon" do
      patch "/v1/pokemons/#{@pokemon.id}", params: { name: "updated" }

      expect(response).to be_successful
      expect(JSON.parse(response.body)["name"]).to match /updated/
    end
  end

  describe "DELETE /v1/pokemons/:id" do
    before {
      @pokemon = create(:pokemon)
    }

    it "should successfully update a pokemon" do
      delete "/v1/pokemons/#{@pokemon.id}"

      expect(response).to be_successful
      expect(Pokemon.all.length).to equal 0
    end
  end

end
