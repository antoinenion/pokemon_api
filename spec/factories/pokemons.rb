require 'faker'

FactoryBot.define do
  factory :pokemon do
    name { Faker::Games::Pokemon.name }
    serial_number { Random.rand(1000) }
    type_1 { Faker::Lorem.word }
    type_2 { Faker::Lorem.word }
    total { Random.rand(100) }
    hit_points { Random.rand(100) }
    attack { Random.rand(100) }
    defense { Random.rand(100) }
    sp_attack { Random.rand(100) }
    sp_defense { Random.rand(100) }
    speed { Random.rand(100) }
    generation { Random.rand(1..6) }
    legendary { true }
  end
end
