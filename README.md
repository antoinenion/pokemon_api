# Pokemon Api
Small CRUD Api to manage pokemons.

## Description
By default, a pokemons list is imported from source file [pokemons_data.csv](https://gitlab.com/antoinenion/pokemon_api/-/blob/master/db/pokemons_data.csv)

## Get Started

### Locally with Docker
1) Build the image
   ```docker build -t pokemon-api .```
2) Launch it
   ```docker run -p 3000:3000 pokemon-api```

### On Heroku
The Api is available from the url [https://jarinux-pokemon-api.herokuapp.com/](https://jarinux-pokemon-api.herokuapp.com/)

## Endpoints

### List pokemons
<b>GET /v1/pokemons</b>

#### Parameters
* per_page : Items by page. Default is 20
* page : Page you are looking. Default is 1

### Show a specific pokemon
<b>GET /v1/pokemons/:id</b>

### Create a pokemon
<b>POST /v1/pokemons</b>

Sample payload :
```
{
    "serial_number": 123,
    "name": "My New Pokemon",
    "type_1": "Fire",
    "type_2": "Flying",
    "total": 534,
    "attack": 84,
    "defense": 78,
    "hit_points": 78,
    "sp_attack": 109,
    "sp_defense": 85,
    "speed": 100,
    "generation": 1,
    "legendary": true
}
```

### Update a pokemon
<b>PATCH /v1/pokemons/:id</b>

Sample payload :
```
{
    "serial_number": 123,
    "name": "My Updated Pokemon",
    "type_1": "Fire",
    "type_2": "Flying",
    "total": 534,
    "attack": 86,
    "defense": 78,
    "hit_points": 78,
    "sp_attack": 109,
    "sp_defense": 85,
    "speed": 100,
    "generation": 1,
    "legendary": false
}
```

### Delete a pokemon
<b>DELETE /v1/pokemons/:id</b>
